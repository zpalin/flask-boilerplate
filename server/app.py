from flask_script import Manager
from flask import Flask, render_template
app = Flask(__name__)
app.config['DEBUG'] = None
app_manager = Manager(app)

app.add_url_rule('/favicon.ico', redirect_to='static/favicon.ico')

@app.route('/')
def index():
    return render_template('index.j2', name="Zac")

if __name__ == "__main__":
    app_manager.run()
