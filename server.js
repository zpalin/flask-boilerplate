const express = require("express");
var compression = require('compression');
const app = express();

app.use(compression());

app.use('/', express.static(__dirname + '/dist'));

app.get('*', function(request, response, next) {
  response.sendfile(__dirname + '/dist/index.html');
});

// app.post('*', function(request, response, next) {
//   response.sendfile(__dirname + '/dist/index.html');
// });

var server = app.listen(80, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Vigilant Frontend running on http://%s:%s', host, port);
});
