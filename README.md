# The greatest workflow the world has ever seen

Steps to greatness:
1. Create virtualenv and activate
2. Run make dev (or pip install -r requirements.txt && npm install && gulp watch)
3. Build the world
